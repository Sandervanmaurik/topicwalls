/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.models;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import javax.json.Json;
import javax.json.JsonObjectBuilder;
import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

/**
 *
 * @author sande
 */
public class PostEncoder implements Encoder.Text<Post> {

    private static Gson gson = new GsonBuilder().create();

    @Override
    public String encode(Post p) throws EncodeException {
        JsonObjectBuilder jsonPoster = Json.createObjectBuilder();
        jsonPoster.add("id", p.getCreator().getId())
                .add("username", p.getCreator().getUsername());
        JsonObjectBuilder json = Json.createObjectBuilder();
        json.add("id", p.getId())
                .add("message", p.getMessage())
                .add("createdAt", p.getCreatedAt())
                .add("creator", jsonPoster);
        String returnValue = json.build().toString();
        return returnValue;

    }

    @Override
    public void init(EndpointConfig ec) {
    }

    @Override
    public void destroy() {
    }

}
