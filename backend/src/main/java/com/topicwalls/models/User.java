/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;


@NamedQueries({
    @NamedQuery(name = "user.all", query = "SELECT u FROM User as u"),
    @NamedQuery(name = "user.userforname", query = "SELECT u FROM User u WHERE u.username = :name"),
    @NamedQuery(name = "user.posts", query = "SELECT u.posts FROM User u WHERE u.id=:id"),
})
@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;

    private String password;

    private String email;

    @OneToMany(mappedBy="creator")
    @JsonIgnore
    private List<Post> posts;
    
    @ManyToMany(mappedBy="members")
    @JsonIgnore
    private List<Wall> subscribedWalls;
    
    public User(String username, String password){
        this.username = username;
        this.password = password;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }

    
    public User() {
        this.subscribedWalls = new ArrayList();
        this.posts = new ArrayList();
    }
    
    public boolean addWall(Wall wall){
        return this.subscribedWalls.add(wall);
    }

    public List<Wall> getSubscribedWalls() {
        return subscribedWalls;
    }

    public void setSubscribedWalls(List<Wall> subscribedWalls) {
        this.subscribedWalls = subscribedWalls;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", username=" + username + ", password=" + password + ", email=" + email + '}';
    }
    
}
