/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.json.bind.annotation.JsonbTransient;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author sande
 */
@NamedQueries({
    @NamedQuery(name = "walls.all", query = "SELECT w FROM Wall as w"),
    @NamedQuery(name = "walls.subscribed", query = "SELECT m FROM Wall as w JOIN w.members as m WHERE m.id = :id")})
@Entity
public class Wall implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @ManyToOne
    private User creator;

    @ManyToMany
    private List<User> members;
    
    @OneToMany
    private List<Post> posts;
        
    public Wall(User creator, String name){
        this.creator = creator;
        this.name = name;
    }
    
    public Wall() {
        this.members = new ArrayList();
        this.posts = new ArrayList();
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
    
    

    public boolean addMember(User user) {
        return this.members.add(user);
    }
    
    public boolean addPost(Post post){
        return this.posts.add(post);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    @Override
    public String toString() {
        return "Wall{" + "id=" + id + ", name=" + name + ", creator=" + creator + ", members=" + members + '}';
    }

}
