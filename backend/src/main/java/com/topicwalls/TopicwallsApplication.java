package com.topicwalls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicwallsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TopicwallsApplication.class, args);
	}

}
