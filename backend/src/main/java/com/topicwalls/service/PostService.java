/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.service;

import com.topicwalls.dao.PostDAO;
import com.topicwalls.dao.UserDAO;
import com.topicwalls.models.Post;
import com.topicwalls.rest.WSResource;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.EncodeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author sande
 */
@Service("postService")
public class PostService {

    @Autowired
    PostDAO postDAO;

    @Autowired
    UserDAO userDAO;

    @Autowired
    public WSResource wsResource;

    public Post findPost(long postId) {
        Post p = this.postDAO.findPost(postId);
        try {
            this.wsResource.broadcast(p);
        } catch (IOException | EncodeException ex) {
            Logger.getLogger(PostService.class.getName()).log(Level.SEVERE, null, ex);
        }
        return p;
    }

    public Post addPost(Post post, long wallId) {
        try {
            Post p = this.postDAO.addPost(post, wallId);
            p.setCreator(this.userDAO.findUser(post.getCreator().getId()).get());
            if (this.wsResource != null) {
                this.wsResource.broadcast(p);
            }
            return p;
        } catch (IOException | EncodeException ex) {
            Logger.getLogger(PostService.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
