/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.service;

import com.sun.org.apache.xalan.internal.lib.ExsltDatetime;
import com.topicwalls.dao.WallDAO;
import com.topicwalls.models.Post;
import com.topicwalls.models.User;
import com.topicwalls.models.Wall;
import com.topicwalls.rest.WSResource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.websocket.EncodeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author sande
 */
@Service("wallService")
public class WallService {
    
    @Autowired
    public WallDAO wallDAO;
    
    @Autowired
    public WSResource wsResource;
    
    public List<Wall> getWalls(){
        return wallDAO.getWalls();
        
    }
    
    public Wall createWall(Wall wall){
        return wallDAO.createWall(wall);
    }

    public List<Wall> getSubscribedWalls(long userId) {
        return wallDAO.getSubscribedWalls(userId);
    }
    
    public boolean subscribeToWall(long wallId, long userId){
        return wallDAO.subscribeToWall(wallId, userId);
    }

    public Wall findWall(Long wallId) {
        return wallDAO.findWall(wallId);
    }

    public List<Post> getPostsForWall(long wallId) {
        return wallDAO.getPostsForWall(wallId);
    }
}
