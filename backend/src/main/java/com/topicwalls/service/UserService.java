/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.service;

import com.topicwalls.dao.UserDAO;
import com.topicwalls.models.Post;
import com.topicwalls.models.User;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author sande
 */
@Service("userService")
public class UserService {

    @Autowired
    UserDAO userDAO;
    

    public User addUser(User user) {
        return this.userDAO.addUser(user);
    }

    public Optional<User> getUser(long userId) {
        return this.userDAO.findUser(userId);
    }

    public User loginUser(String username, String password) {
        return this.userDAO.loginUser(username, password);
    }
    
    public Optional<User> getUserForUsername(String username){
        return this.userDAO.getUserForUsername(username);
    }

    public boolean existsByUsername(String username) {
        return this.userDAO.existsByUsername(username);
    } 
    
}
