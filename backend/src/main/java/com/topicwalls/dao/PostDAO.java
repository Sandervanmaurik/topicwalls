/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.dao;

import com.topicwalls.models.Post;
import com.topicwalls.models.Wall;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

/**
 *
 * @author sande
 */
@Service("postDAO")
public class PostDAO {

    @PersistenceContext
    private EntityManager em;

    public Post findPost(long postId) {
        return this.em.find(Post.class, postId);
    }

    @Transactional
    public Post addPost(Post post, long wallId) {
        Wall wall = em.find(Wall.class, wallId);
        if (wall != null) {
            wall.addPost(post);
            post.setWall(wall);
            this.em.persist(post);
            em.persist(wall);
        }
        System.out.println(post);
        return em.find(Post.class, post.getId());
    }
}
