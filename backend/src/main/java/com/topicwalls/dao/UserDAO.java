/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.dao;

import com.topicwalls.models.Post;
import com.topicwalls.models.User;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;

@Service("userDAO")
public class UserDAO {

    @PersistenceContext
    private EntityManager em;

    public Optional<User> findUser(long userId) {
        return Optional.of(this.em.find(User.class, userId));
    }

    @Transactional
    public User addUser(User user) {
        em.persist(user);
        em.flush();
        return user;
    }

    public User loginUser(String username, String password) {
        try {
            Query q = em.createNamedQuery("user.userforname");
            User user = (User) q.setParameter("name", username).getSingleResult();
            if (user.getPassword().equals(password)) {
                return user;
            }
        } catch (NoResultException ex) {
            return null;
        }
        return null;
    }

    public Optional<User> getUserForUsername(String username) {
        Query q = em.createNamedQuery("user.userforname");
        User user = (User) q.setParameter("name", username).getSingleResult();
        return Optional.of(user);
    }

    public boolean existsByUsername(String username) {
        Query q = em.createNamedQuery("user.userforname");
        return q.setParameter("name", username).getResultList().size() == 1;
    }
}
