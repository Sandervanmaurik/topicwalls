/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.dao;

import com.topicwalls.models.Post;
import com.topicwalls.models.User;
import com.topicwalls.models.Wall;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author sande
 */
@Service("wallDAO")
public class WallDAO {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UserDAO userDAO;

    public List<Wall> getWalls() {
        return new ArrayList<>(em.createNamedQuery("walls.all").getResultList());
    }

    public Wall findWall(long wallId) {
        Wall wall = this.em.find(Wall.class, wallId);
        return wall;
    }

    @Transactional
    public Wall createWall(Wall wall) {
        wall.addMember(wall.getCreator());
        User user = userDAO.findUser(wall.getCreator().getId()).get();
         user.addWall(wall);
        em.persist(wall);
        return wall;
    }

    @Transactional
    public boolean subscribeToWall(long wallId, long userId) {
        Wall wall = this.findWall(wallId);
        User user = this.userDAO.findUser(userId).get();
        if (wall != null && user != null) {
            wall.addMember(user);
            user.addWall(wall);
            em.merge(wall);
            em.flush();
            return true;
        }
        System.out.println("Wall or User is null");
        return false;
    }

    @Transactional
    public List<Wall> getSubscribedWalls(long userId) {
        User u = em.find(User.class, userId);
        
        if (u != null) {
            return u.getSubscribedWalls();
        }
        System.out.println("No user");
        return null;
    }

    public List<Post> getPostsForWall(long wallId) {
       Wall wall = em.find(Wall.class, wallId);
       if(wall != null){
           return wall.getPosts();
       }
        System.out.println("getPostsForWall: wall = null");
        return null;
    }
}
