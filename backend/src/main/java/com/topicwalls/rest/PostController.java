/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.rest;

import com.topicwalls.models.Post;
import com.topicwalls.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sande
 */
@RestController
@CrossOrigin
@RequestMapping("/posts")
public class PostController {

    @Autowired
    public PostService postService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Post findPost(@RequestHeader("postId") Long postId) {
        return this.postService.findPost(postId);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public Post addPost(@RequestBody Post post, @RequestHeader long wallId) {
        return this.postService.addPost(post, wallId);
    }

}
