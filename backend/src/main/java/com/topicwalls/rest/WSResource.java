/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.rest;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.topicwalls.models.Post;
import com.topicwalls.models.PostEncoder;
import com.topicwalls.models.User;
import com.topicwalls.service.WallService;
import javax.inject.Singleton;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author sande
 */
@Singleton
@ServerEndpoint(value = "/ws",
        encoders = PostEncoder.class)
public class WSResource {

    private Session session;
    private static Set<WSResource> wsEndpoints = new CopyOnWriteArraySet<>();
    private User user;
    
    
    @Autowired
    WallService wallService;

    @OnOpen
    public void onOpen(Session session) throws IOException {
        System.out.println("Opened connection");
    }

    @OnMessage
    public void onMessage(Session session, String message) throws IOException {
        System.out.println("Receiving message");
        Gson gson = new Gson();
        try {
            User u = gson.fromJson(message, User.class);
            System.out.println(wsEndpoints.stream().filter(o -> o.user.equals(user)).findFirst().isPresent());
            if (!wsEndpoints.stream().filter(o -> o.user.equals(u)).findFirst().isPresent()) {
                this.session = session;
                this.user = u;
                wsEndpoints.add(this);
            }
        } catch (JsonSyntaxException e) {
            System.out.println(e);
        }

    }

    @OnClose
    public void onClose(Session session) throws IOException {
        WSResource endpoint = null;

        for (WSResource res : wsEndpoints) {
            if (res.session == session) {
                endpoint = res;
                break;
            }
        }

        wsEndpoints.remove(endpoint);
    }

    @OnError
    public void onError(Session session, Throwable throwable) {

    }

    public void broadcast(Post post) throws IOException, EncodeException {
        System.out.println(wsEndpoints.size());
        for (WSResource endpoint : wsEndpoints) {
            System.out.println("post.userid" + post.getCreator().getId());
            System.out.println("enpoint.user.getid=" + endpoint.user.getId());
            System.out.println("endpoint.user.walls=" + this.wallService.getSubscribedWalls(endpoint.user.getId()));
            System.out.println("post.wall=" + post.getWall());
            if (endpoint.user.getId().equals(post.getCreator().getId()) || this.wallService.getSubscribedWalls(endpoint.user.getId()).contains(post.getWall())) {
                endpoint.session.getBasicRemote().sendObject(post);
            }
        }
    }
}
