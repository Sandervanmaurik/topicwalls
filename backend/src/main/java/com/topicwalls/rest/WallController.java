/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.rest;

import com.topicwalls.models.Post;
import com.topicwalls.models.Wall;
import com.topicwalls.service.WallService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author sande
 */
@RestController
@CrossOrigin
@RequestMapping("/walls")
public class WallController {

    @Autowired
    WallService wallService;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Wall> getWalls() {
        return this.wallService.getWalls();
    }

    @RequestMapping(value = "/wall", method = RequestMethod.GET)
    public Wall findWall(@RequestHeader("wallId") Long wallId) {
        return this.wallService.findWall(wallId);
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public Wall createWall(@RequestBody Wall wall) {
        return wallService.createWall(wall);
    }

    @RequestMapping(value = "/subscribed", method = RequestMethod.GET)
    public List<Wall> getSubscribedWalls(@RequestHeader("userId") long userId) {
        return this.wallService.getSubscribedWalls(userId);
    }

    @RequestMapping(value = "/subscribe", method = RequestMethod.POST)
    public boolean subscribeToWall(@RequestHeader("wallId") long wallId, @RequestHeader("userId") long userId) {
        return wallService.subscribeToWall(wallId, userId);
    }
     @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public List<Post> getPostsForWall(@RequestHeader("wallId") long wallId) {
        return this.wallService.getPostsForWall(wallId);
    }
}
