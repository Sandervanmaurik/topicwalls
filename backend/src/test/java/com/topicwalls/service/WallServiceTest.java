/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.service;

import com.topicwalls.dao.PostDAO;
import com.topicwalls.dao.UserDAO;
import com.topicwalls.dao.WallDAO;
import com.topicwalls.models.Post;
import com.topicwalls.models.User;
import com.topicwalls.models.Wall;
import java.util.Optional;
import org.junit.After;
import org.junit.Assert;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import static org.mockito.Mockito.when;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

@RunWith(MockitoJUnitRunner.Silent.class)
public class WallServiceTest {

    private Wall wall;
    private User user1;
    private User user2;

    @Mock
    private WallDAO wallDAO;

    @Mock
    private UserDAO userDAO;

    @Mock
    private PostDAO postDAO;

    @InjectMocks
    private WallService wallService;
    @InjectMocks
    private UserService userService;
    @InjectMocks
    private PostService postService;

    public WallServiceTest() {
    }

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.user1 = new User("pietje", "password");
        this.user1.setId(Long.valueOf(1));

        this.user2 = new User("henk", "password");
        this.user2.setId(Long.valueOf(2));
        this.wall = new Wall(user1, "FirstWall");
        this.wall.setId(Long.valueOf(1));
    }

    @After
    public void tearDown() {
    }

    @Test
    public void createWalLTest() {
        when(this.wallDAO.createWall(wall)).thenReturn(wall);
        Wall wall1 = this.wallService.createWall(this.wall);
        assertNotNull(wall1);
        assertEquals(Long.valueOf(1), wall1.getId());
    }

    @Test
    public void getUserByIdTest() {
        Optional<User> optionalUser = Optional.of(this.user1);
        when(this.userDAO.findUser(1)).thenReturn(optionalUser);
        User user = this.userService.getUser(1).get();
        assertEquals(user, this.user1);

    }

    @Test
    public void subscribeToWallTest() {
        when(this.wallDAO.subscribeToWall(this.wall.getId(), this.user1.getId())).thenReturn(true);
        assertEquals(this.wallService.subscribeToWall(this.wall.getId(), this.user1.getId()), true);
    }

    @Test
    public void addPostTest() {
        this.wall = new Wall(user1, "FirstWall");
        this.wall.setId(Long.valueOf(1));
        Post p = new Post();
        p.setId(Long.valueOf(1));
        p.setCreator(user1);
        when(this.userDAO.findUser(this.user1.getId())).thenReturn(Optional.of(this.user1));
        when(this.postDAO.addPost(p, this.wall.getId())).thenReturn(p);

        Post post = this.postService.addPost(p, this.wall.getId());

        assertNotNull(post);
        assertEquals(post.getCreator(), this.user1);
    }
}
