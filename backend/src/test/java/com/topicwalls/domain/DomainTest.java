/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.topicwalls.domain;

import com.topicwalls.models.User;
import com.topicwalls.models.Wall;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author sande
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class DomainTest {

    private Wall wall;
    private User user1;
    private User user2;

    @Autowired
    private TestEntityManager entityManager;

    public DomainTest() {
    }

    @Before
    public void setUp() {
        this.user1 = new User("pietje", "password");
        this.user2 = new User("henk", "password");
        wall = new Wall();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void addUserTest() {
        user1 = entityManager.persistAndFlush(user1);
        user2 = entityManager.persistAndFlush(user2);

        assertEquals(Long.valueOf(1), user1.getId());
        assertEquals(Long.valueOf(2), user2.getId());
    }

    @Test
    public void addWallTest() {
        wall = entityManager.persistAndFlush(wall);
        assertEquals(Long.valueOf(3), wall.getId());
    }
}
