import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { WallService } from 'src/app/services/wall.service';
import { Post } from 'src/app/models/post';
import { Wall } from 'src/app/models/wall';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  currentWall: Wall;
  walls: Wall[] = [];
  user: User;
  constructor(private dataService: DataService, private router: Router, private wallService: WallService) { }

  ngOnInit() {
    if (this.dataService.getSessionId() == null) {
      this.router.navigateByUrl("login");
    }
    this.user = new User(this.dataService.getUserId());
    this.dataService.wallsData.subscribe(walls => {
      this.user.subscribedWalls = walls as Wall[];
    });
    this.dataService.selectedWall.subscribe(wall => {
      this.currentWall = wall;
    });
    this.dataService.wallsData.subscribe(response => {
      this.walls = response;
    });
  }
}
