import { Component, OnInit, NgZone } from '@angular/core';
import { Wall } from 'src/app/models/wall';
import { DataService } from 'src/app/services/data.service';
import { WallService } from 'src/app/services/wall.service';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { Post } from 'src/app/models/post';
import { WallComponent } from '../wall/wall.component';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  subscribedWalls: Wall[];
  currentWall: Wall;
  user: User;
  constructor(private dataService: DataService, private wallService: WallService, private router: Router, private ngZone: NgZone) { }

  ngOnInit() {
    this.user = new User(this.dataService.getUserId());
    this.dataService.selectedWall.subscribe(wall => {
      this.currentWall = wall;
    })
    this.dataService.wallsData.subscribe(walls => {
      this.user.subscribedWalls = walls as Wall[];
    });

    if (this.dataService.getSessionId() != null) {
      this.wallService.getWallsForUser(this.user.id).subscribe(walls => {
        this.user.subscribedWalls = walls as Wall[];
        this.dataService.changeSubscribedWalls(this.user.subscribedWalls);
      });
      this.openWSConnection();
    }
  }
  openWSConnection() {
    this.wallService.openWSConnection(onmessage => {
      let messageEvent: MessageEvent = onmessage as MessageEvent;
      let post: Post = JSON.parse(messageEvent.data);
      console.log("RECEIVING FROM WS....");
      this.currentWall.posts.push(post);

      this.dataService.changeSelectedWall(this.currentWall);
    }, onopen => {
      console.log("Opening WS connection...");
      this.wallService.registerWSUser(this.user);
    });
  }

  openWall(wall: Wall) {
    this.dataService.changeSelectedWall(wall);
    this.router.navigateByUrl('/walls/' + wall.id);
  }

  loginUser(userId) {
    this.dataService.changeSelectedWall(new Wall());
    this.dataService.changeSubscribedWalls([]);

    this.dataService.setSessionId(userId);
    this.wallService.getWallsForUser(userId).subscribe(response => {
      this.user.subscribedWalls = response as Wall[];
      this.dataService.changeSubscribedWalls(response as Wall[]);
      this.router.navigateByUrl("");
    });
  }
  logout() {
    this.dataService.deleteSession();
    this.router.navigateByUrl('');
  }
}
