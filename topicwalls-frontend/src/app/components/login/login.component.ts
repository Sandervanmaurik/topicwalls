import { Component, OnInit, NgZone } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Wall } from 'src/app/models/wall';
import { WallService } from 'src/app/services/wall.service';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
declare var M: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username;
  password;

  constructor(private dataService: DataService, private wallService: WallService
    , private router: Router, private route: ActivatedRoute, private ngZone: NgZone) { }

  ngOnInit() {
    if (this.dataService.getSessionId() != null) {
      this.router.navigateByUrl('');
    }
    this.dataService.changeSelectedWall(new Wall());
    this.dataService.changeSubscribedWalls([]);
  }

  loginUser(username, password) {
    this.dataService.loginUser(username, password).subscribe(user => {
      if (user != null) {
        this.dataService.setSessionId(user["body"]["accessToken"]);
        console.log(this.dataService.getUserId());
        this.router.navigateByUrl('');
      }
    }, error => {
      console.log(error);
      M.toast({ html: 'Inloggegevens zijn onjuist', classes: "rounded" });
    });
  }
  registerUser(username, password) {
    this.dataService.registerUser(username, password).subscribe(response => {
      console.log(response);
      M.toast({ html: "Geregistreerd!", classes: "rounded" });
    }, error => {
      console.log(error)
      M.toast({ html: 'Dit voldoet niet aan de eisen!', classes: "rounded" });
    });
  }

}
