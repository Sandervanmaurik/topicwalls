import { Component, OnInit, NgZone } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Wall } from 'src/app/models/wall';
import { Post } from 'src/app/models/post';
import { WallService } from 'src/app/services/wall.service';
import { User } from 'src/app/models/user';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-wall',
  templateUrl: './wall.component.html',
  styleUrls: ['./wall.component.scss']
})
export class WallComponent implements OnInit {

  wall: Wall;
  message;
  constructor(private dataService: DataService, private wallService: WallService,
    private router: Router, private ngZone: NgZone, private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (this.dataService.getSessionId() == null) {
      this.router.navigateByUrl("login");
    }
    this.dataService.selectedWall.subscribe(wall => {
      this.wall = wall as Wall;

    });

    this.route.paramMap.subscribe(params => {
      this.wall.id = params.get("id");
    });
    if (this.wall != null) {
      this.wallService.getWall(this.wall.id).subscribe(response => {
        this.wall = response as Wall;
        this.wallService.getPostsForWall(this.wall.id).subscribe(posts => {
          this.wall.posts = posts as Post[];
        });
        this.dataService.changeSelectedWall(response as Wall);
      });
    }
  }

  postMessage() {
    let post = new Post();
    post.createdAt = Date.now().toLocaleString();
    post.creator = new User(this.dataService.getUserId());
    post.message = this.message;
    post.wall = this.wall;
    this.wallService.postMessageOnWall(post, this.wall.id).subscribe(response => {
    });
  }
}
