import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { MaterializeModule } from "@samuelberthe/angular2-materialize";
import { Routes, RouterModule } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HttpClientModule } from '@angular/common/http';
import { WallService } from './services/wall.service';
import { DataService } from './services/data.service';
import { WallComponent } from './components/wall/wall.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './components/login/login.component';
import { JwtModule } from '@auth0/angular-jwt';

export function tokenGetter() {
  return localStorage.getItem('session');
}

const routes: Routes = [
  { path: 'walls/:id', component: WallComponent },
  { path: "login", component: LoginComponent },
  { path: '', component: HomeComponent },

];
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    WallComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterializeModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes),
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: ['localhost:4200', 'localhost', 'localhost:8080']
      }
    }),

  ],
  entryComponents: [
    WallComponent
  ],
  providers: [
    WallService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
