import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Wall } from '../models/wall';
import { HttpParams, HttpHeaders, HttpClientModule, HttpClient } from '@angular/common/http';
import { LoginRequest } from '../models/loginRequest';
import { RegisterRequest } from '../models/registerRequest';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private wallsDataSource = new BehaviorSubject<Wall[]>([]);
  wallsData = this.wallsDataSource.asObservable();

  private selectedWallDataSource = new BehaviorSubject<Wall>(new Wall());
  selectedWall = this.selectedWallDataSource.asObservable();

  public changeSubscribedWalls(walls: Wall[]) {
    this.wallsDataSource.next(walls);
  }
  public changeSelectedWall(wall: Wall) {
    this.selectedWallDataSource.next(wall);
  }

  public getSessionId() {
    return localStorage.getItem("session");
  }

  public setSessionId(id: any) {
    localStorage.setItem("session", id);
  }

  public getUserId(): string {
    if(this.jwtHelperService.tokenGetter() != null){
      return this.jwtHelperService.decodeToken(this.jwtHelperService.tokenGetter()).sub;
    }
    return null;
  }


  public deleteSession() {
    localStorage.removeItem("session");
  }
  public loginUser(username, password) {
    let headers = new HttpHeaders();
    headers = headers.set("username", username);
    headers = headers.set("password", password);
    let loginRequest: LoginRequest = new LoginRequest();
    loginRequest.username = username;
    loginRequest.password = password;
    return this.http.post("http://localhost:8080/auth/login", loginRequest, { headers: headers, observe: 'response' });
  }

  public registerUser(username, password) {
    let registerRequest: RegisterRequest = new RegisterRequest();
    registerRequest.username = username;
    registerRequest.password = password;
    return this.http.post("http://localhost:8080/auth/register", registerRequest, { observe: 'response' });
  }

  constructor(private http: HttpClient, private jwtHelperService: JwtHelperService) { }
}
