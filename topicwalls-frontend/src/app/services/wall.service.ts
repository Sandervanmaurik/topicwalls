import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Post } from '../models/post';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class WallService {

  private connection: WebSocket;

  constructor(private http: HttpClient) {

  }
  public getWallsForUser(userId) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    headers = headers.set("userId", userId);
    return this.http.get("http://localhost:8080/walls/subscribed", { headers: headers });
  }

  public getWall(wallId) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    if (wallId) {
      headers = headers.set("wallId", wallId);
    }
    return this.http.get("http://localhost:8080/walls/wall", { headers: headers });
  }

  public postMessageOnWall(post: Post, wallId) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    headers = headers.set("wallId", wallId);
    return this.http.post("http://localhost:8080/posts/", post, { headers: headers });
  }

  public getPostsForWall(wallId) {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
    });
    headers = headers.set("wallId", wallId);
    return this.http.get("http://localhost:8080/walls/posts", { headers: headers });
  }

  public openWSConnection(onmessage, onOpenListener) {
    this.connection = new WebSocket("ws://localhost:8080/ws");
    this.connection.onopen = onOpenListener
    this.connection.onmessage = onmessage;
  }

  public registerWSUser(user: User) {
    this.connection.send(JSON.stringify(user));
  }
}
