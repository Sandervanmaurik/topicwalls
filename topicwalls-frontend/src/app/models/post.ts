import { User } from './user';
import { Wall } from './wall';

export class Post{
    public id;
    public creator: User;
    public createdAt: string;
    public message: string;
    public wall: Wall;

}