import { Wall } from './wall';

export class User{
    public id;
    public username;
    public password;

    public subscribedWalls: Wall[];

    constructor(id){
        this.id = id;
    }
}