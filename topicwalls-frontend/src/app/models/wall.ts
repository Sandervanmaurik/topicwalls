import { User } from './user';
import { Post } from './post';

export class Wall{
    public id;
    public name;
    public members: User[] = [];
    public posts: Post[] = [];

}